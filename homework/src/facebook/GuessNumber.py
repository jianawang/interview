# The guess API is already defined for you.
# @param num, your guess
# @return -1 if my number is lower, 1 if my number is higher, otherwise return 0
# def guess(num):

class Solution(object):
    def guessNumber(self, n):
        """
        :type n: int
        :rtype: int
        """
        # number: 1->n
        # index:  0->n-1
        start = 1
        end = n
        while (start + 1 < end):
            mid = (start + (end - start)) / 2
            if self.guess(mid) == 0:
                return mid
            elif self.guess(mid) < 0:
                end = mid-1
            else:
                start = mid+1

        #start==end or start+1==end
        if start == end:
            return start if self.guess(start)==0 else 0
        else:
            if self.guess(start) == 0:
                return start
            elif self.guess(end) == 0:
                return end
            else:
                return 0

    # 0,1,-1
    def guess(self,num):
        return 0