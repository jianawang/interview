class Solution(object):

    def multiply(self, num1, num2):
        """
        :type num1: str
        :type num2: str
        :rtype: str
        """
        sum_per_position = {}
        sum_per_position[0] = 0
        # i in num1, j position in num2, index in sum per position is (i+j)
        for i in range(0, len(num1)):
            for j in range(0, len(num2)):
                product = int(num1[len(num1) - i - 1]) * int(num2[len(num2) - j - 1])
                start = i + j
                while product>0:
                    if start not in sum_per_position:
                        sum_per_position[start] = 0
                    sum_per_position[start] += product
                    remain = sum_per_position[start] % 10
                    carry = int(sum_per_position[start]/10)
                    sum_per_position[start] = remain
                    start += 1
                    product = carry
        result = ""
        max_key = max(sum_per_position.keys())

        for i in range(0, max_key + 1):
            key = max_key - i
            if key in sum_per_position:
                result += (str(sum_per_position[key]))
            else:
                result += '0'
        return result

a = Solution()
print(a.multiply("6","501"))
