matrix = []


########################################################################
for i in range(4):
    one_row = []
    for j in range(6):
        if i < 2 and j > 0 and j<3:
            one_row.append(0)
        elif i == 2 and j == 0:
            one_row.append(0)
        elif i >= 2 and i<=3 and j >= 4 and j <= 5:
            one_row.append(0)
        else:
            one_row.append(1)
    matrix.append(one_row)

for row in range(len(matrix)):
    print(matrix[row])

########################################################################
def find_corners(matrix):
    # every row
    for i in range(len(matrix)):
        # every column
        for j in range(len(matrix[0])):
            if matrix[i][j] == 0:
                max_x = find_max_x_index(i, j, matrix)
                max_y = find_max_y_index(i,j, matrix)
                return ([i, j],[max_x,max_y])

def find_max_x_index(i, j, matrix):
    runner = i
    while runner <= len(matrix)-1 and matrix[runner][j]==0:
        runner = runner + 1
    return runner-1

def find_max_y_index(i, j, matrix):
    runner = j
    while runner <= len(matrix[0])-1 and matrix[i][runner]==0:
        runner = runner + 1
    return runner-1

def create_all_false(matrix):
    visited = [[False for j in range(len(matrix[0]))] for i in range(len(matrix))]
    return visited

def mark_visited(i_low, i_high, j_low, j_high, visited):
    for i in range(i_low, i_high + 1):
        for j in range(j_low, j_high + 1):
            visited[i][j] = True

def find_corners_from_multiple_matrics(matrix):
    visited = create_all_false(matrix)
    print(visited)
    min_x_result = len(matrix)-1
    max_x_result = 0

    for i in range(len(matrix)):
        for j in range(len(matrix[0])):
            if matrix[i][j] == 0 and not visited[i][j]:
                if i < min_x_result:
                    min_x_result = i
                max_x = find_max_x_index(i, j, matrix)
                max_y = find_max_y_index(i,j, matrix)
                if max_x > max_x_result:
                    max_x_result = max_x
                mark_visited(i, max_x, j, max_y, visited)

    return (min_x_result,max_x_result)

def number_of_islands(matrix):
    visited = create_all_false(matrix)
    islands = 0
    for i in range(len(matrix)):
        for j in range(len(matrix[0])):
            # 1. find a new island
            if not visited[i][j] and matrix[i][j] == 0:
                islands=islands+1
                queue = []
                queue.append([i, j])
                while len(queue) > 0:
                    element = queue.pop(0)
                    add_to_queue(visited, matrix, element[0] - 1, element[1], queue)
                    add_to_queue(visited, matrix, element[0] + 1, element[1], queue)
                    add_to_queue(visited, matrix, element[0], element[1]-1, queue)
                    add_to_queue(visited, matrix, element[0], element[1]+1, queue)
    return islands


def add_to_queue(visited, matrix, i, j, queue):
    if i < 0 or i >= len(matrix) or j < 0 or j >= len(matrix[0]) or visited[i][j] or matrix[i][j]==1:
        return
    queue.append([i, j])
    visited[i][j] = True

print(find_corners_from_multiple_matrics(matrix))
print(number_of_islands(matrix))