# https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=579156&highlight=karat

import math
words1 = [ "The", "day", "began", "as", "still", "as", "the", "night", "abruptly", "lighted", "with", "brilliant", "flame" ]

####1
def wrapLines(words,maxLength):
    res = [words[0]]
    for word in words[1:]:
        lastLine = res[-1]
        if len(lastLine) + len(word)+1<=maxLength:
            res[-1] +="-"+ word
        else:
            res.append(word)
    return res
# print(wrapLines(words1,13))
# print(wrapLines(words1,20))

###2
def fillLine(line,maxLength):
    words = line.split("-")
    wordsLen = len("".join(words))
    leftLen = maxLength-wordsLen
    if len(words) == 1:
        return line
    floorNum = math.floor(leftLen/(len(words)-1))
    dashes = [floorNum] * (len(words)-1)
    i = 0
    while sum(dashes) != leftLen:
        dashes[i]+=1
        i+=1
    res = ""
    for j in range(len(words)-1):
        res += words[j]+dashes[j-1] * "-"
    res += words[-1]
    return res

def wrapLines2(words,maxLength):
    simpleOne = wrapLines(words,maxLength)
    res = []
    for line in simpleOne:
        filledOne = fillLine(line,maxLength)
        res.append(filledOne)
    return res
print(wrapLines2(words1,13))
