class Solution(object):
    def findMin(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        start, end = 0, len(nums) - 1
        while (start <= end):
            mid = start + int((end - start) / 2)
            if mid == start:  # only two element left
                return min(nums[start],nums[end])
            if nums[start] < nums[mid]:
                if nums[start] > nums[end]:
                    start = mid + 1
                else:
                    end = mid - 1
            else:
                if nums[mid - 1] < nums[mid]:
                    end = mid - 1
                else:
                    return nums[mid]
        return -1
