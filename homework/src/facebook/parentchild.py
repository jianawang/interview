# """ 第一题：
# Suppose we have some input data describing a graph of relationships between parents and children over multiple generations.
# The data is formatted as a list of (parent, child) pairs, where each individual is assigned a unique integer identifier.
# For example, in this diagram, 3 is a child of 1 and 2, and 5 is a child of 4:

# parentChildPairs = [  (1, 3), (2, 3), (3, 6), (5, 6),
#                                (5, 7), (4, 5), (4, 8), (8, 10)  ]
# Write a function that takes this data as input and returns two collections: one containing all individuals with zero known parents, and one containing all individuals with exactly one known parent.
# findNodesWithZeroAndOneParents(parentChildPairs) =>
#                                               [ [1, 2, 4],    // Individuals with zero parents
#                                               [5, 7, 8, 10] // Individuals with exactly one parent ]


# 第二题：
# Write a function that takes the graph, as well as two of the individuals in our dataset, as its inputs and returns true if and only if they share at least one ancestor.
# Sample input and output: （input和上面一样）
# hasCommonAncestor(parentChildPairs, 3, 8) => false
# hasCommonAncestor(parentChildPairs, 5, 8) => true
# hasCommonAncestor(parentChildPairs, 6, 8) => true
# hasCommonAncestor(parentChildPairs, 1, 3) => false


# 第三题：

# For example, in this diagram, 3 is a child of 1 and 2, and 5 is a child of 4
# Write a function that, for a given individual in our dataset, returns their earliest known ancestor -- the one at the farthest distance from the input individual. . check 1point3acres for more.
# If there is more than one ancestor tied for "earliest", return any one of them. If the input individual has no parents, the function should return null (or -1).
# Sample input and output:
# parent_child_pairs = [ (1, 3), (2, 3), (3, 6), (5, 6), (5, 7), (4, 5), (4, 8), (8, 10), (11, 2) ]
# . 1point3acres
# output
# findEarliestAncestor(parentChildPairs, 8) => 4
# findEarliestAncestor(parentChildPairs, 7) => 4
# findEarliestAncestor(parentChildPairs, 6) => 11
# findEarliestAncestor(parentChildPairs, 1) => null or -1

# https://www.1point3acres.com/bbs/thread-453770-1-1.html

# # For example, in this diagram, 3 is a child of 1 and 2, and 5 is a child of 4:

# # 1   2   4
# #  \ /   / \
# #   3   5   8
# #    \ / \   \
# #     6   7   9

# # ancestors = [6]
# # ancestors = [3,5]
# # ancestors = [3,4]
# # ancestors = [3]
# # ancestors = [1,2]

# # Write a function that takes this data as input and returns two collections: one containing all individuals with zero known parents, and one containing all individuals with exactly one known parent.

# # Sample output (pseudocode):
# # [
# #   [1, 2, 4],   // Individuals with zero parents
# #   [5, 7, 8, 9] // Individuals with exactly one parent
# # ]

# # Write a function that, for two given individuals in our dataset, returns true if and only if they share at least one ancestor.

# # Sample input and output:
# # parentChildPairs, 3, 8 => false
# # parentChildPairs, 5, 8 => true
# # parentChildPairs, 6, 8 => true


# # Write a function that, for a given individual in our dataset, returns their earliest known ancestor -- the one at the farthest distance from the input individual. If there is more than one ancestor tied for "earliest", return any one of them. If the input individual has no parents, the function should return null (or -1).

# # Sample input and output:


# # 8 => 4
# # 7 => 4
# # 6 => 1, 2, or 4


# parent_child_pairs = [
#     (1, 3), (2, 3), (3, 6), (5, 6),
#     (5, 7), (4, 5), (4, 8), (8, 9)
# ]

# from collections import defaultdict

# def child_with_parents(edges):
#     """
#     edges: List[Tuple[int,int]]
#     return: List[List[int]]
#     """
#     nodes = set()
#     parent_count = defaultdict(int)
#     for parent, child in edges:
#         parent_count[child] += 1
#         nodes.add(parent)
#     zero_parents = [node for node in nodes if node not in parent_count]
#     one_parents = [child for child, count in parent_count.items() if count == 1]
#     return [zero_parents, one_parents]


# # print(child_with_parents(parent_child_pairs))


# def has_common_ancestor(pairs, a, b):
#     parents = defaultdict(list)
#     for parent, child in pairs:
#         parents[child].append(parent)
#     def ancestors(node):
#         """yielding all ancestors"""
#         for parent in parents[node]:
#             yield parent
#             yield from ancestors(parent)
#     ancestors_a = set(ancestors(a))
#     return any(ancestor in ancestors_a for ancestor in ancestors(b))



# # print(has_common_ancestor(parent_child_pairs, 3,8))
# # print(has_common_ancestor(parent_child_pairs, 5,8))
# # print(has_common_ancestor(parent_child_pairs, 6,8))



# def earliest_ancestor(pairs, a):
#     parents = defaultdict(list)
#     for parent, child in pairs:
#         parents[child].append(parent)
#     def earilest_ancestor(node, node_gen):
#         """return (gen, earilest_ancestor): (int, int)"""
#         best_gen, best_ancestor = node_gen, node
#         for parent in parents[node]:
#             gen, ancestor = earilest_ancestor(parent, node_gen+1)
#             if  gen is not None and \
#                 (best_gen is None or \
#                 gen > best_gen):
#                 best_gen, best_ancestor = gen, ancestor
#         return best_gen, best_ancestor
#     gen, ancestor = earilest_ancestor(a, 0)
#     if a == ancestor:
#         return None
#     return ancestor

# for i in range(1, 10):
#     print(i, earliest_ancestor(parent_child_pairs, i))



# [/size][/backcolor]
# [backcolor=rgb(255, 247, 160)][size=12px]





# ###1 """
def findNodesWithZeroAndOneParents(list):
    res = {}
    for i,j in list:
        res[i]=0
        res[j]=0
    for i,j in list:
        res[j] += 1
    zeroParent,oneParent = [],[]
    for node,numOfParents in res.items():
        if numOfParents == 0:
            zeroParent.append(node)
        elif numOfParents == 1:
            oneParent.append(node)
    return [zeroParent,oneParent]


###2
def previousNodes(map,node):
    q = [node]
    res = set()
    while q:
        node1 = q.pop()
        try:
            res.update(set(map[node1]))
            q.extend(map[node1])		##can be improved by adding visited sets
        except:
            pass
    return res


def hasCommonAncestor(list,node1,node2):
    parentChildMap = {}
    for parent,child in list:
        if child in parentChildMap:
            parentChildMap[child].append(parent)
        else:
            parentChildMap[child] = [parent]
    ##use bfs here for both node1 and node2
    previous1 = previousNodes(parentChildMap,node1)
    previous2 = previousNodes(parentChildMap,node2)
    return len(previous1.intersection(previous2)) >0

####3

parent_child_pairs = [ (1, 3), (2, 3), (3, 6), (5, 6), (5, 7), (4, 5), (4, 8), (8, 10), (11, 2),(11,4),(12,1)]
def previousNodesWithDistance(mapp,node):
    ### {distance from node: (nodes)}
    resNode = -1
    maxDis = 0
    q = [(node,0)]
    while q:
        node1,dis = q.pop()
        if node1 in mapp:
            nextNodes = mapp[node1]
            if dis+1>maxDis:
                maxDis = dis+1
                resNode = nextNodes[0]
            q.extend([[node,dis+1] for node in nextNodes])
    return resNode


def task3(listt,node):
    parentChildMap = {}
    for parent,child in listt:
        if child in parentChildMap:
            parentChildMap[child].append(parent)
        else:
            parentChildMap[child] = [parent]
    return previousNodesWithDistance(parentChildMap,node)

print(task3(parent_child_pairs,8))
print(task3(parent_child_pairs,7))
print(task3(parent_child_pairs,6))
print(task3(parent_child_pairs,1))
print(task3(parent_child_pairs,10))
