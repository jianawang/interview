class Solution(object):
    def multiply(self, A, B):
        """
        :type A: List[List[int]]
        :type B: List[List[int]]
        :rtype: List[List[int]]
        """
        columnA = len(A)
        rowA = len(A[0])  # == len(B)
        rowB = len(B[0])
        result = []
        print(A)
        print(B)
        for i in range(0, columnA):
            result.append([])
            for j in range(0, rowB):
                result[i].append(0)

        for i in range(0, columnA):
            for j in range(0, rowA):
                for k in range(0, rowB):
                    result[i][k] += (A[i][j] * B[j][k])
        return result

A = [
  [ 1, 0, 0],
  [-1, 0, 3]
]

B = [
  [ 7, 0, 0 ],
  [ 0, 0, 0 ],
  [ 0, 0, 1 ]
]
a = Solution()
print(a.multiply(A,B))



