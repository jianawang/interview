"""
给一个board，0表示可以走的地方，-1表示墙，不能穿墙也不能停留在墙的格子。可以上下左右四个方向移动。

1. 写一个function，输入是board和一定在board内的一个点的坐标，输出是从这个点出发一次移动之内可能到达的点。
就是这么简单，差不多是给你热身的意思，问清楚要求和条件，排除掉不能走和超出边界的点就行。

2. 还是这样的board，输入是board和board内一个点，写function判断其余所有点是否都能到达这个点，
返回true/false。还是很简单，bfs判断一下是不是所有0都connected就行，我基本上一遍就过了test cases。
跑完之后面试官问了下复杂度。但是因为我写得不够快，到第三题的时候45分钟只剩5、6分钟的样子了。

3. 现在board里还有第三种点treasure，用1表示，function输入是board、起始点和终点，
要求：计算从起点到终点并能经过所有treasure的最短路径，如果有多条返回任意一条即可，如果不存在就返回None。
一开始我不懂为什么会有None的情况，题目里也没有明说，后来看了example猜想是因为不允许走回头路。
examples如下：
// board3 = [
//     [  1,  0,  0, 0, 0 ],
//     [  0, -1, -1, 0, 0 ],
//     [  0, -1,  0, 1, 0 ],
//     [ -1,  0,  0, 0, 0 ],
//     [  0,  1, -1, 0, 0 ],
//     [  0,  0,  0, 0, 0 ],
// ]


// treasure(board3, (5, 0), (0, 4)) -> None

// treasure(board3, (5, 2), (2, 0)) ->
//   [(5, 2), (5, 1), (4, 1), (3, 1), (3, 2), (2, 2), (2, 3), (1, 3), (0, 3), (0, 2), (0, 1), (0, 0), (1, 0), (2, 0)]
//   Or
//   [(5, 2), (5, 1), (4, 1), (3, 1), (3, 2), (3, 3), (2, 3), (1, 3), (0, 3), (0, 2), (0, 1), (0, 0), (1, 0), (2, 0)]

// treasure(board3, (0, 0), (4, 1)) ->
//   [(0, 0), (0, 1), (0, 2), (0, 3), (1, 3), (2, 3), (2, 2), (3, 2), (3, 1), (4, 1)]
//   Or
//   [(0, 0), (0, 1), (0, 2), (0, 3), (1, 3), (2, 3), (3, 3), (3, 2), (3, 1), (4, 1)]
 """

import copy

board2 = [
    [1, 0, 0, 0, 0],
    [0, -1, -1, 0, 0],
    [0, -1, 0, 1, 0],
    [-1, 0, 0, 0, 0],
    [0, 1, -1, 0, 0],
    [0,0,0,0,0],
]

board3 = [
    [1, 0, 0, 0, 0],
    [0, -1, -1, 0, 0],
    [0, -1, 0, 1, 0],
    [-1, 0, 0, 0, 0],
    [0, 1, -1, 0, 0],
    [0,0,0,0,0],
]

# i is row index (up,down), j is column index(left,right)

# question 1
def is_out_of_board(board, i, j):
    return i < 0 or i >= len(board) or j < 0 or j >= len(board[0]) or board[i][j] ==-1

def get_available_position_with_shift(i, j , i_shift, j_shift, board):
    result = []
    while not is_out_of_board(board, i, j):
        result.append((i,j))
        i=i+i_shift
        j=j+j_shift
    return result

def get_all_places(board, i, j):
    result = set()
    result.update(get_available_position_with_shift(i,j,0,1, board))
    result.update(get_available_position_with_shift(i,j,0,-1, board))
    result.update(get_available_position_with_shift(i,j,-1,0, board))
    result.update(get_available_position_with_shift(i,j,1,0, board))
    return result

print("-"*20)
print(get_all_places(board3,0,0))
print("-"*20)

# question 2
def visit_if_new(board, i, j, result, visited):
    if not is_out_of_board(board, i, j) and not visited[i][j]:
        visited[i][j] = True
        result.append((i,j))

def add_movable_spots(board, i, j,positions_to_move,visited):
    visit_if_new(board, i-1, j, positions_to_move,visited)
    visit_if_new(board, i+1, j, positions_to_move,visited)
    visit_if_new(board, i, j-1, positions_to_move,visited)
    visit_if_new(board, i, j + 1, positions_to_move,visited)
    return positions_to_move

def is_all_connected(board, m, n):

    # set cache
    visited = [[False for j in range(len(board[0]))] for i in range(len(board))]
    visited[m][n] = True

    # bfs
    queue = []
    queue.append((m, n))
    while len(queue) > 0:
        i, j = queue.pop(0)
        add_movable_spots(board, i, j,queue,visited)

    # check if all position has been visited
    for i in range(len(board)):
        for j in range(len(board[0])):
            if not visited[i][j] and board[i][j] == 0:
                return False
    return True

print("X"*20)
print(is_all_connected(board3,0,1))
print("X"*20)

# 3.
# Find all permutations of possible visiting sequences starting from the start,
# pass every treasure, and ending at end.
#
# For each pair of nodes next to each other, find the shortest path.
# Also, for each permutation, store all visited cells in a set so as not to revisit it.

def find_shorted_path(board, x, y, m, n):

    result_paths = []
    # boundary check
    if is_out_of_board(board, x, y) or is_out_of_board(board, m, n):
        return result_path

    # 1. find total treasure count
    total_treasure_count = 0
    for i in range(len(board)):
        for j in range(len(board[0])):
            total_treasure_count += 1 if board[i][j] == 1 else 0

    # 3. bfs from start to end, when reaches treasure, increment each counter
    queue = [[(x,y)]]
    while len(queue) > 0:

        # get last element
        one_path = queue.pop(0)
        i, j=one_path[-1]

        # found the end!
        # only the paths reach to the end consider as one candidate
        if i == m and j == n:
            result_paths.append(copy.copy(one_path))

        # keep expanding on the paths
        else:
            # add the path back to queue, if it has other places to go
            add_if_not_visited(board, i - 1, j, copy.copy(one_path), queue)
            add_if_not_visited(board, i + 1, j, copy.copy(one_path), queue)
            add_if_not_visited(board, i, j-1, copy.copy(one_path), queue)
            add_if_not_visited(board, i, j + 1, copy.copy(one_path), queue)

    result = None
    for path in result_paths:
        treasure_seen = 0

        # 1. go through all point in the path
        for point in path:
            if board[point[0]][point[1]] == 1:
                treasure_seen +=1

        # 2. find a path
        if treasure_seen == total_treasure_count:
            # 2.1 set max
            if result is None:
                result = path
            # 2.2 find max
            elif len(result) > len(path):
                result = path
    return result

def add_if_not_visited(board, i, j, one_path, queue):
    if not is_out_of_board(board, i, j) and (i,j) not in one_path:
        one_path.append((i,j))
        queue.append(one_path)

print("---"*30)
print(find_shorted_path(board3, 5, 0, 0, 4))
print("---"*30)
print(find_shorted_path(board3, 5, 2, 2, 0))
print("---"*30)
print(find_shorted_path(board3,0,0,4,1))
