class BSTIterator(object):

    def __init__(self, root):
        """
        :type root: TreeNode
        """
        self.stack = []
        self.add_left_branch_for(root)

    def add_left_branch_for(self,node):
        while node is not None:
            self.stack.append(node)
            node = node.left

    def next(self):
        """
        @return the next smallest number
        :rtype: int
        """
        if not self.hasNext():
            return -1
        item = self.stack.pop()
        self.add_left_branch_for(item.right)
        return item.val

    def hasNext(self):
        """
        @return whether we have a next smallest number
        :rtype: bool
        """
        return len(self.stack)>0