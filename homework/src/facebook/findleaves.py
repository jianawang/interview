def findLeaves(self, root):
    """
    :type root: TreeNode
    :rtype: List[List[int]]
    """

    if root is None:
        return []

    ans = []
    leaves = []

    def traverse(node):

        if node is None:
            return True

        if node.left is None and node.right is None:
            leaves.append(node.val)
            return True

        if traverse(node.left) is True:
            node.left = None

        if traverse(node.right) is True:
            node.right = None

        return leaves

    while True:
        if root.left is None and root.right is None:
            ans.append([root.val])
            break

        leaves = []
        traverse(root)
        ans.append(leaves)

    return ans

