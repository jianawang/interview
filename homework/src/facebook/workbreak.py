class Solution(object):
    def wordBreak(self, s, wordDict):
        return self.workBreakWithMemo(s,len(s)-1,wordDict,{})

    def workBreakWithMemo(self, s, index, wordDict, memo):
        if index == 3:
            print('s')
            pass
        if len(s) == 0:
            return False
        if index in memo:
            return memo[index]

        for j in range(0, index):  # 0->index-1
            if self.workBreakWithMemo(s, j, wordDict, memo) and s[j+1:index+1] in wordDict:
                result = memo[index] = True
                return True
        result = s[:index+1] in wordDict    # whold word
        memo[index] = result
        return result

s = Solution()
wordDict = ['leet','code']
print(s.wordBreak("leetcode", wordDict))

