# Content, user ID, timestamp
user_content_likes = [
    ["http://yahoo.com", "user3", "201999"],
    ["http://google.com/maps", "user2", "201004"],
    ["http://google.com/maps", "user1", "201015"],
    ["http://google.com", "user4", "201004"],
    ["http://google.com", "user2", "201012"],
    ["http://google.com/maps", "user2", "201008"],
    ["http://google.com/maps", "user2", "201013"],
    ["http://google.com/maps", "user2", "201030"],
    ["http://altavista.net/q?f=12344", "user3", "100002"],
    ["http://google.com/maps", "user3", "201015"],
    ["http://yahoo.com", "user2", "201035"],
    ["http://altavista.net/q?f=12344", "user1", "99999"],
    ["http://altavista.net/q?f=12344", "user1", "100004"],
    ["http://geocities.com", "user1", "100007"],
    ["http://geocities.com", "user3", "100009"]
]

def find_similar_user(user_content_likes, lookup):
    site_users_map = {}
    # 1. "site" -> {user1,user2}
    for like in user_content_likes:
        site, user, _ = like
        if site_users_map.get(site) is None:
            site_users_map[site] = set()
        site_users_map[site].add(user)

    # 2. {user: {user,freq}}
    similarity_map = {}
    for site,user_list in site_users_map.items():
        populate_similar_map(user_list, similarity_map)

    # 3. find top one
    friendReqMap = similarity_map[lookup]
    max_freq = 0
    most_similar = user
    for frd,freq in friendReqMap.items():
        if freq > max_freq:
            max_freq = freq
            most_similar = frd
    return most_similar

def populate_similar_map(list_of_users, map):
    for user1 in list_of_users:
        for user2 in list_of_users:
            if user1 != user2:
                if map.get(user1) is None:
                    map[user1] = {}
                freinds_freq = map[user1]
                if freinds_freq.get(user2) is None:
                    freinds_freq[user2] = 0
                freinds_freq[user2]+=1

print("--"*10)
print(find_similar_user(user_content_likes, "user1"))
print(find_similar_user(user_content_likes, "user2"))
print(find_similar_user(user_content_likes, "user3"))
print(find_similar_user(user_content_likes,"user4"))