# You are a developer for a university. Your current project is to develop a system for
# students to find courses they share with friends. The university has a system for querying
# courses students are enrolled in, returned as a list of (ID, course) pairs.

# Write a function that takes in a list of (student ID number, course name) pairs and returns,
# for every pair of students, a list of all courses they share.

# Sample Input:

student_course_pairs_1 = [
  ["58", "Software Design"],
  ["58", "Linear Algebra"],
  ["94", "Art History"],
  ["94", "Operating Systems"],
  ["17", "Software Design"],
  ["58", "Mechanics"],
  ["58", "Economics"],
  ["17", "Linear Algebra"],
  ["17", "Political Science"],
  ["94", "Economics"],
  ["25", "Economics"],
]

student_course_pairs_2 = [
  ["42", "Software Design"],
  ["0", "Advanced Mechanics"],
  ["9", "Art History"],
]

def find_pairs(student_course_list):
    # 1. build cource -> students cache
    course_student_map = {}
    student_set = set()
    for student_course in student_course_list:
        student, course = student_course
        student_set.add(student)
        if course_student_map.get(course) is None:
            course_student_map[course] = set()
        course_student_map[course].add(student)

    # 2. generate student pairs
    student_course_map = {}
    for s1 in student_set:
        for s2 in student_set:
            if s1 != s2:
                s = min(s1, s2)
                b = max(s1,s2)
                if student_course_map.get((s,b)) is None:
                    student_course_map[(s,b)] = []

    # 3. from course, build pairs
    for course, students in course_student_map.items():
        add_course_to_student_map(students, student_course_map, course)

    return student_course_map

def add_course_to_student_map(students, student_course_map, course):
    for s1 in students:
        for s2 in students:
            if s1 != s2:
                s = min(s1, s2)
                b = max(s1, s2)
                if course not in student_course_map[(s, b)]:
                    student_course_map[(s,b)].append(course)


#print(find_pairs(student_course_pairs_1))
print(find_pairs(student_course_pairs_2))


##2
# 告诉你a是b的先修课，b是c的先修课，问你mid course是啥。并且条件是，只有一种修下来课的顺序，这里是 a -> b ->c
# 所以mid course就是b
