import random
class Solution(object):

    def __init__(self, w):
        """
        :type w: List[int]
        """
        max_int = 0
        self.range_index = {}
        for i in range(0, len(w)):
            value = w[i]
            max_int = max(max_int, value)
            for v in range(1,value+1):
                if v not in self.range_index:
                    self.range_index[v] = []
                self.range_index[v].append(i)


    def pickIndex(self):
        value = random.choice(key_list)
        index = random.choice(self.range_index[value])
        return index

a = Solution([1,3])
print(a.pickIndex())
print(a.pickIndex())
print(a.pickIndex())

