import heapq

class Solution(object):
    def findKthLargest(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: int
        """
        h = []
        for num in nums:
            heapq.heappush(h,num)
            if len(h)>0:
                h=heapq.nlargest(k,h)
        return  heapq.nlargest(k,h)[-1]