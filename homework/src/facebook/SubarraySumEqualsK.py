class Solution(object):
    def subarraySum(self, nums, k):
        sum_freq = {}
        sum_freq[0] = 1
        sum_so_far = 0
        result = 0
        for n in nums:
            sum_so_far = n + sum_so_far

            look_for = sum_so_far - k
            if look_for in sum_freq:
                result += sum_freq[look_for]

            if sum_so_far in sum_freq:
                sum_freq[sum_so_far] += 1
            else:
                sum_freq[sum_so_far] = 1
        return result
