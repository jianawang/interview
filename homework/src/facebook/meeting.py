# 1.给一组non-overlapped 的meeting intervals，再给一个interval，
# 返回该interval是否跟那组meeting intervals有任何overlap
# 2. 给多个人一天的meeting schedules，找出所有intervals使得所有人在这些intervals内都是空闲的。
# 地里面经都有，只要合并所有人的meeting schedules，然后找出每两个schedules之间的interval就是所有人都空闲的interval。
# 需要注意的是这个问题考虑的是一整天的时间，即从0:00-23:59，所以如果合并后的schedules中最早的开始时间晚于0:00，
# 那么从0:00到开始这段时间也是空闲时间。
# 同理，如果最晚的结束时间早于23:59，那么从结束到23:59这段时间也是空闲时间。


intervals1 =[[1300, 1500], [930, 1200],[830, 845],[820,830]]

###1
def insertable(intervals,newInterval):
    start,end = newInterval
    for existStart,existEnd in intervals:
        if existStart<start<existEnd or existStart<end<existEnd:
            return False
    return True

# print(insertable(intervals1,[820,830]))
# print(insertable(intervals1,[1450,1500]))

###2
def mergeIntervals(intervals):
    intervals.sort(key = lambda x: x[0])
    res = [intervals[0]]
    print(res)
    for interval in intervals[1:]:
        ostart,oend = res[-1]
        if ostart<= interval[0] <= oend:
            res[-1][1] = max(oend,interval[1])
        else:
            res.append(interval)
    return res

print(mergeIntervals(intervals1))

def freeTime(intervals):
    busyTime = mergeIntervals(intervals)
    res = [[0,busyTime[0][0]]]
    for i in range(len(busyTime)-1):
        res.append([busyTime[i][1],busyTime[i+1][0]])
    res.append([busyTime[-1][1],2400])
    return res

print(freeTime(intervals1))

###3
rooms = {
  "Phone Booth":     {"size":  2},
  "War Room":        {"size":  6},
  "Conference Room": {"size": 12}
}

meetings1 = {
  "Standup": {"size":  4, "start": 1230, "end": 1300},
  "Scrum":   {"size":  6, "start": 1230, "end": 1330},
  "UAT":     {"size": 10, "start": 1300, "end": 1500}
}

meetings2 = {
  "Manager 1:1": {"size": 2, "start":  900, "end": 1030},
  "Budget":      {"size": 4, "start":  900, "end": 1000},
  "Forecasting": {"size": 6, "start":  900, "end": 1100},
  "SVP 1:1":     {"size": 2, "start": 1000, "end": 1030},
  "UX Testing":  {"size": 4, "start": 1015, "end": 1130}
}

def task3(roomsDict,meetingsDict):
    rooms,meetings = [],[]
    res = {}
    for room,info in roomsDict.items():
        rooms.append([room,info["size"],[]])
    for meeting,info in meetingsDict.items():
        meetings.append([meeting,info["size"],[info["start"],info["end"]]])
    rooms.sort(key = lambda x: x[1])
    meetings.sort(key = lambda x: x[1],reverse=True)
    for meeting in meetings:
        complete = False
        for room in rooms:
            if room[1]>=meeting[1] and insertable(room[2],meeting[2]) and not complete:
                room[2].append(meeting[2])
                res[meeting[0]] = room[0]
                complete = True
        if not complete:
            return "Impossible"

    return res