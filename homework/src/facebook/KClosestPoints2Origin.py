from queue import PriorityQueue

class Solution(object):
    def kClosest(self, points, K):
        """
        :type points: List[List[int]]
        :type K: int
        :rtype: List[List[int]]
        """
        q = PriorityQueue(K)
        for p in points:
            print(p)
            q.put(Node(p[0],p[1]))
        result = []
        return q

class Node(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.distance = pow(self.x, 2) + pow(self.y, 2)

    def __cmp__(self, other):
        return cmp(self.distance, other.distance)

    def distance(self):
        return pow(self.x,2) + pow(self.y,2)

s = Solution()
print(s.kClosest([[1,3], [-2,2]],1))
