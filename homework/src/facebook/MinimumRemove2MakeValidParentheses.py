class Solution(object):
    def minRemoveToMakeValid(self, s):
        """
        :type s: str
        :rtype: str
        """
        stack_left = []
        list_right = []
        for i in range(0, len(s)):
            if s[i] == '(':
                stack_left.append(i)
            if s[i] == ')':
                if len(stack_left) > 0:
                    stack_left.pop()
                else:
                    list_right.append(i)
        result = []
        for i in range(0, len(s)):
            if i not in stack_left and i not in list_right:
                result.append(s[i])
        return result


