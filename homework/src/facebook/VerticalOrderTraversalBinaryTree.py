class Solution(object):
    def verticalTraversal(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
        queue = []
        result = {}
        key = []
        if root is not None:
            queue.append((root,0))
        while len(queue) > 0:
            node,pos = queue.pop(0)
            if pos not in result:
                result[pos] = []
            result[pos].append(node.val)
            if node.left is not None:
                queue.append((node.left, pos - 1))
            if node.right is not None:
                queue.append((node.right, pos + 1))
        r = []
        keys = result.keys()
        keys.sort()
        for key in keys:
            r.append(result[key])
        return r