class Solution(object):
    def leastInterval(self, tasks, n):
        """
        :type tasks: List[str]
        :type n: int
        :rtype: int
        """
        freq = {}
        for task in tasks:
            if task not in freq:
                freq[task] = 0
            freq[task] += 1
        l = freq.values()
        l.sort(reverse=True)
        # AAA -> 2
        seq_count = l.pop(0)-1
        idle = seq_count * n

        while len(l)>0:
            idle -= min(seq_count, l.pop(0))
        # maybe no idle slots left
        return (idle if idle>0 else 0) + len(tasks)