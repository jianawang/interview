user_items = [
    ("u1", "i1"),
    ("u1", "i2"),
    ("u2", "i3"),
    ("u3", "i4"),
]
ads_ips = [
    ("ads1", "ip1"),
    ("ads1", "ip2"),
    ("ads2", "ip3"),
    ("ads4", "ip1")
]

ip_user_map = {}
ip_user_map["ip1"] = "u1"
ip_user_map["ip2"] = "u2"
ip_user_map["ip3"] = "u1"
ip_user_map["ip4"] = "u3"

def unique_users_per_ads(ads_ips, ip_user_map):
    unique_user_per_ads_map = {}
    clicked_ads_user = set()
    for ad_ip in ads_ips:
        (ad, ip) = (ad_ip[0], ad_ip[1])
        if unique_user_per_ads_map.get(ad) is None:
            unique_user_per_ads_map[ad] = set()
        user = ip_user_map.get(ip)
        if user is not None:
            unique_user_per_ads_map[ad].add(user)
            clicked_ads_user.add(user)
    return unique_user_per_ads_map, clicked_ads_user

def purchased_users_and_clicked_users(user_items, clicked_ads_user):
    purchased_users = set()
    for user_item in user_items:
        user, item = user_item
        purchased_users.add(user)
    return set.intersection(purchased_users,clicked_ads_user)



unique_user_per_ads_map,clicked_ads_user = unique_users_per_ads(ads_ips, ip_user_map)
print("hello world1")
print(unique_user_per_ads_map)
print(clicked_ads_user)
print("intersection")
inter = purchased_users_and_clicked_users(user_items, clicked_ads_user)
print(inter)