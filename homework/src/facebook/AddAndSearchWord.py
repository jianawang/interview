class WordDictionary(object):

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.root = TrieNode()


    def addWord(self, word):
        """
        Adds a word into the data structure.
        :type word: str
        :rtype: None
        """
        r = self.root
        for c in word:
            if c not in r.children.keys():
                r.children[c] = TrieNode()
            r = r.children[c]
        r.isWord = True

    def search(self, word):
        """
        Returns if the word is in the data structure. A word could contain the dot character '.' to represent any one letter.
        :type word: str
        :rtype: bool
        """
        return self.search2(word,0,self.root)

    def search2(self, word, index, node):
        if index == len(word):
            return node.isWord
        if node is None:
            return False

        c = word[index]

        if c == '.':
            for child in node.children.keys():
                if self.search2(word, index + 1, node.children[child]):
                    return True
            return False
        else:
            if c not in node.children:
                return False
            else:
                print("hihihi")
                return self.search2(word,index+1,node.children[c])


class TrieNode(object):
    def __init__(self):
        self.children = {}
        self.isWord = False

a = WordDictionary()
a.addWord("ab")
print(a.search("a."))
# Your WordDictionary object will be instantiated and called as such:
# obj = WordDictionary()
# obj.addWord(word)
# param_2 = obj.search(word)