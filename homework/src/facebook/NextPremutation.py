class Solution(object):
    def nextPermutation(self, nums):
        """
        :type nums: List[int]
        :rtype: None Do not return anything, modify nums in-place instead.
        """
        if len(nums) <= 1:
            return nums

        i = len(nums)-2 #index of interest
        while (i>=0 and nums[i] > nums[i+1]):
            i = i - 1
        # decreasing
        if i == -1:
            nums.reverse()
            return nums

        # find the point to change is i
        swap_index = i+1
        for j in range(i + 1, len(nums)):
            if nums[j] < nums[i]:
                break
            if nums[j] < nums[swap_index]:
                swap_index = j
        # swap
        temp = nums[i]
        nums[i] = nums[swap_index]
        nums[swap_index] = temp

        # reorder
        result = []
        result.extend(nums[: i + 1])
        result2 = nums[i + 1 :]
        result2.reverse()
        result.extend(result2)
        return result

a = Solution()
print(a.nextPermutation([1,3,2]))
