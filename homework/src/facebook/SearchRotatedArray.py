class Solution(object):
    def search(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        start = 0
        end = len(nums) - 1
        while (start <= end):
            mid = start + int((end - start) / 2)
            if nums[mid] == target:
                return mid
            else:
                # only two element
                if nums[start] == nums[mid]:
                # 1. left contiouns
                    return mid if nums[mid]==target else (end if nums[end]==target else -1)
                elif nums[start] < nums[mid]:
                    if target > nums[mid] or target < nums[start]:
                        start = mid + 1
                    else:
                        end = mid - 1
                # 2. right contiouns nums[mid]<nums[end]
                else:
                    if target < nums[mid] or target >= nums[start]:
                        end = mid - 1
                    else:
                        start = mid +1
        return - 1

a = Solution()
print(a.search([3,1],int(1)))