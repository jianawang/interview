class Solution(object):
    def exclusiveTime(self, n, logs):
        """
        :type n: int
        :type logs: List[str]
        :rtype: List[int]
        """
        execute_time = {}
        for i in range(n):
            execute_time[i] = 0
        stack = []  # pid
        progress = 0
        for log in logs:
            # "0:start:3" -> "1:start:4"
            p1,p2,p3 = log.split(":")
            pid, operation, ts = int(p1),p2,int(p3)
            if operation == "start":
                if (len(stack) > 0):
                    execute_time[stack[-1]] += ts - progress
                progress = ts
                stack.append(pid)
            # end, update total
            else:
                pid = stack.pop()
                execute_time[pid] += (ts - progress + 1)
                progress = ts + 1
                print(execute_time)

        return execute_time.values()

logs =["0:start:0","1:start:5","2:start:6","3:start:9","4:start:11","5:start:12","6:start:14","7:start:15","1:start:24","1:end:29","7:end:34","6:end:37","5:end:39","4:end:40","3:end:45","0:start:49","0:end:54","5:start:55","5:end:59","4:start:63","4:end:66","2:start:69","2:end:70","2:start:74","6:start:78","0:start:79","0:end:80","6:end:85","1:start:89","1:end:93","2:end:96","2:end:100","1:end:102","2:start:105","2:end:109","0:end:114"]
n = 8
a = Solution()
print(a.exclusiveTime(n,logs))
