import heapq

class Solution(object):
    def reorganizeString(self, S):
        """
        :type S: str
        :rtype: str
        """
        # -3,-2,-1
        pq = [(-S.count(x), x) for x in set(S)]
        heapq.heapify(pq)
        if (-pq[0][0]) * 2 - 1 > len(S):
            return ""
        result = []
        while len(pq) >= 2:
            c1, l1 = heapq.heappop(pq) # count,letter
            c2, l2 = heapq.heappop(pq)
            result.extend([l1, l2])
            if c1+1<0: heapq.heappush(pq,(c1+1,l1))     # count,letter
            if c2+1 < 0: heapq.heappush(pq, (c2 + 1, l2))  # count,letter
        if len(pq) > 0:
            result.extend(heapq.heappop(pq)[1])
        return "".join(result)

a = Solution()
raw = "vvvlo"
print(a.reorganizeString(raw))
