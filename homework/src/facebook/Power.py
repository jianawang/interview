class Solution(object):
    def myPow(self, x, n):
        """
        :type x: float
        :type n: int
        :rtype: float
        """
        if n < 0:
            x = 1 / x
            n = -n
        # pow(x,8) = pow(x,4) * pow(x,4)
        # pow(x,9) = pow(x,4) * pow(x,4) * * pow(x,1)
        cache = {}
        cache[1] = x
        cache[0] = 1
        return self.myPow2(x,n,cache)

    def myPow2(self, x, n, cache):
        if n in cache:
            return cache[n]
        mod = n % 2
        half = int(n / 2)
        result = self.myPow2(x,half,cache) * self.myPow2(x,half,cache) * self.myPow2(x,mod,cache)
        cache[n] = result
        return result

a = Solution()
print(a.myPow(2,4))
