#Input: [[1,3],[2,6],[8,10],[15,18]]
#Output: [[1,6],[8,10],[15,18]]
#Explanation: Since intervals [1,3] and [2,6] overlaps, merge them into [1,6].

class Solution(object):
    def merge(self, intervals):
        """
        :type intervals: List[List[int]]
        :rtype: List[List[int]]
        """
        if intervals==[]:
            return []
        intervals.sort(key=lambda x: x[0])
        result = []
        start,end = intervals[0]
        for meeting in intervals[1:]:
            current_start, current_end = meeting
            # merge
            if end >= current_start:
                end = max(current_end,end)
            else:
                result.append([start, end])
                start,end = current_start,current_end
        result.append([start, end])
        return result
