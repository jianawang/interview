""" logest continuous common history
给每个user访问历史记录，找出两个user之间longest continuous common history
输入： [
             ["3234.html", "xys.html", "7hsaa.html"], // user1
             ["3234.html", ''sdhsfjdsh.html", "xys.html", "7hsaa.html"] // user2
           ], user1 and user2 （指定两个user求intersect）

输出：["xys.html", "7hsaa.html"]

用dp做
 """

def longest(x, y):
    result = []
    lch = []

    # 1. create
    for i in range(len(x)):
        one_list = []
        for j in range(len(y)):
            one_list.append([])
        lch.append(one_list)

    # 2. init
    for j in range(len(y)):
        lch[0][j] = [] if x[0] != y[j] else [x[0]]
    for i in range(len(x)):
        lch[i][0] = [] if x[i] != y[0] else [y[0]]

    # 3. loop
    for i in range(1,len(x)):
        for j in range(1, len(y)):
            if x[i] == y[j]:
                lch[i][j] = lch[i - 1][j - 1]
                lch[i][j].append(x[i])
            else:
                lch[i][j]= []

            if len(result) < len(lch[i][j]):
                result = lch[i][j]
    return result

input_1 = ["a", "b"]
input_2 = ["a", "b", "c"]
print(longest(input_1, input_2))

input_3 = ["3234", "xyz","7hsaa"]
input_4 = ["3234", "sdssd","xyz","7hsaa"]
print(longest(input_3, input_4))

input_5 = ["a", "b","c","d","e"]
input_6 = ["a", "b","x","c","d","e"]
print(longest(input_5, input_6))

input_7 = ["a", "b","c","k","e"]
input_8 = ["a", "b","x","c","d","e"]
print(longest(input_7, input_8))