class Solution:
    def isLegit(self, s, start, end):
        return start >= 0 and end <= len(s) and s[start:end].isnumeric() and s[start]!='0' and \
            int(s[start:end])>0 and int(s[start:end])<=26

    def numDecodingWithMemo(self, s, end, memo):
        # reached to the very beginning
        if end == -1:
            return 1
        if end in memo:
            return memo[end]

        one_back = self.numDecodingWithMemo(s, end - 1, memo) if self.isLegit(s,end, end+1) else 0
        two_back = self.numDecodingWithMemo(s, end-2, memo) if self.isLegit(s,end-1,end+1) else 0
        memo[end] = one_back + two_back
        return one_back + two_back

    def numDecodings(self, s):
        return self.numDecodingWithMemo(s, len(s)-1, {})

d = Solution()
print(d.numDecodings("01"))