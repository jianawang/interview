# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

"""
:type root: TreeNode
:rtype: List[List[int]]
"""


def subnodes(nodes):
    re =[]
    for node in nodes:
        if node.left: re.append(node.left)
        if node.right: re.append(node.right)
    return re

def levelOrderBottom(self, root):
    result = []
    if not root: return result
    q = [root]
    while len(q) > 0:
        result.insert(0, [x.val for x in q])
        nodes = subnodes(q)
        q = nodes
    return result