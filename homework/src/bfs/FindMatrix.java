package bfs;

public class FindMatrix {

    public FindMatrix() {
    }

    public Location[] findLocation() {
        Location l = new Location(0, 0);
        Location r = new Location(1, 1);
        Location result[] = new Location[2];
        result[0] = l;
        result[1] = r;



        return result;
    }


    public static void main(String[] args) {
        int[][] matrix = new int[4][6];
        FindMatrix f = new FindMatrix();
        System.out.println(f.findLocation()[0].x + " " +f.findLocation()[0].y);
        System.out.println(f.findLocation()[1].x + " " +f.findLocation()[1].y);
    }

    public class Location {
        int x;
        int y;

        public Location(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
}